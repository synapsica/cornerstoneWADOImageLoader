import external from '../../../externalModules.js';
import getNumberValues from './getNumberValues.js';
import parseImageId from '../parseImageId.js';
import dataSetCacheManager from '../dataSetCacheManager.js';
import getImagePixelModule from './getImagePixelModule.js';
import getOverlayPlaneModule from './getOverlayPlaneModule.js';
import getLUTs from './getLUTs.js';
import getModalityLUTOutputPixelRepresentation from './getModalityLUTOutputPixelRepresentation.js';

function metaDataProvider(type, imageId) {
  const { dicomParser } = external;
  const parsedImageId = parseImageId(imageId);

  const dataSet = dataSetCacheManager.get(parsedImageId.url);

  if (!dataSet) {
    return;
  }

  if (type === 'generalSeriesModule') {
    return {
      modality: dataSet.string('x00080060'),
      seriesInstanceUID: dataSet.string('x0020000e'),
      seriesNumber: dataSet.intString('x00200011'),
      studyInstanceUID: dataSet.string('x0020000e'),
      seriesDescription: dataSet.string('x0008103e'),
      seriesDate: dicomParser.parseDA(dataSet.string('x00080021')),
      seriesTime: dicomParser.parseTM(dataSet.string('x00080031') || ''),
    };
  }

  if (type === 'patientStudyModule') {
    return {
      patientAge: dataSet.intString('x00101010'),
      patientSize: dataSet.floatString('x00101020'),
      patientWeight: dataSet.floatString('x00101030'),
    };
  }

  if (type === 'imagePlaneModule') {
    const imageOrientationPatient = getNumberValues(dataSet, 'x00200037', 6);
    const imagePositionPatient = getNumberValues(dataSet, 'x00200032', 3);
    const pixelSpacing = getNumberValues(dataSet, 'x00280030', 2);

    let columnPixelSpacing = null;

    let rowPixelSpacing = null;

    // Check if Multiframe
    const numberOfFrames = dataSet.intString('x00280008');
    const frameOfReferenceUID = dataSet.string('x00200052');
    const rows = dataSet.uint16('x00280010');
    const columns = dataSet.uint16('x00280011');

    //Invalid Multiframe if no SharedFunctionalGroupsSequence (dataSet.elements.x52009229).
    if (
      numberOfFrames > 0 &&
      frameOfReferenceUID &&
      rows &&
      columns &&
      dataSet.elements.x52009229
    ) {
      // Get multiframe image metadata
      const frameImageInfo = getPerFrameImagePlane(imageId, dataSet);
      return {
        frameOfReferenceUID: frameImageInfo.frameOfReferenceUID,
        rows: frameImageInfo.rows,
        columns: frameImageInfo.columns,
        imageOrientationPatient: frameImageInfo.imageOrientationPatient,
        rowCosines: frameImageInfo.rowCosines,
        columnCosines: frameImageInfo.columnCosines,
        imagePositionPatient: frameImageInfo.imagePositionPatient,
        sliceThickness: dataSet.floatString('x00180050'),
        sliceLocation: dataSet.floatString('x00201041'),
        pixelSpacing: frameImageInfo.pixelSpacing,
        rowPixelSpacing: frameImageInfo.rowPixelSpacing,
        columnPixelSpacing: frameImageInfo.columnPixelSpacing,
      };
    }

    // Metadata for Non multiframe images
    if (pixelSpacing) {
      rowPixelSpacing = pixelSpacing[0];
      columnPixelSpacing = pixelSpacing[1];
    }

    let rowCosines = null;

    let columnCosines = null;

    if (imageOrientationPatient) {
      rowCosines = [
        parseFloat(imageOrientationPatient[0]),
        parseFloat(imageOrientationPatient[1]),
        parseFloat(imageOrientationPatient[2]),
      ];
      columnCosines = [
        parseFloat(imageOrientationPatient[3]),
        parseFloat(imageOrientationPatient[4]),
        parseFloat(imageOrientationPatient[5]),
      ];
    }

    return {
      frameOfReferenceUID,
      rows,
      columns,
      imageOrientationPatient,
      rowCosines,
      columnCosines,
      imagePositionPatient,
      sliceThickness: dataSet.floatString('x00180050'),
      sliceLocation: dataSet.floatString('x00201041'),
      pixelSpacing,
      rowPixelSpacing,
      columnPixelSpacing,
    };
  }

  if (type === 'imagePixelModule') {
    return getImagePixelModule(dataSet);
  }

  if (type === 'modalityLutModule') {
    return {
      rescaleIntercept: dataSet.floatString('x00281052'),
      rescaleSlope: dataSet.floatString('x00281053'),
      rescaleType: dataSet.string('x00281054'),
      modalityLUTSequence: getLUTs(
        dataSet.uint16('x00280103'),
        dataSet.elements.x00283000
      ),
    };
  }

  if (type === 'voiLutModule') {
    const modalityLUTOutputPixelRepresentation = getModalityLUTOutputPixelRepresentation(
      dataSet
    );

    return {
      windowCenter: getNumberValues(dataSet, 'x00281050', 1),
      windowWidth: getNumberValues(dataSet, 'x00281051', 1),
      voiLUTSequence: getLUTs(
        modalityLUTOutputPixelRepresentation,
        dataSet.elements.x00283010
      ),
    };
  }

  if (type === 'sopCommonModule') {
    return {
      sopClassUID: dataSet.string('x00080016'),
      sopInstanceUID: dataSet.string('x00080018'),
    };
  }

  if (type === 'petIsotopeModule') {
    const radiopharmaceuticalInfo = dataSet.elements.x00540016;

    if (radiopharmaceuticalInfo === undefined) {
      return;
    }

    const firstRadiopharmaceuticalInfoDataSet =
      radiopharmaceuticalInfo.items[0].dataSet;

    return {
      radiopharmaceuticalInfo: {
        radiopharmaceuticalStartTime: dicomParser.parseTM(
          firstRadiopharmaceuticalInfoDataSet.string('x00181072') || ''
        ),
        radionuclideTotalDose: firstRadiopharmaceuticalInfoDataSet.floatString(
          'x00181074'
        ),
        radionuclideHalfLife: firstRadiopharmaceuticalInfoDataSet.floatString(
          'x00181075'
        ),
      },
    };
  }

  if (type === 'overlayPlaneModule') {
    return getOverlayPlaneModule(dataSet);
  }
}

function getPerFrameImagePlane(imageId, dataSet) {
  const frameImageMetadata = {};

  frameImageMetadata.rows = dataSet.uint16('x00280010');
  frameImageMetadata.columns = dataSet.uint16('x00280011');
  frameImageMetadata.frameOfReferenceUID = dataSet.string('x00200052');

  const frame = imageId.split('?frame=')[1];

  const SharedFunctionalGroupsSequenceDataSet =
    dataSet.elements.x52009229.items[0].dataSet;

  let PerFrameFunctionalGroupsSequenceI;

  // If the PerFrameFunctionalGroupsSequence entry exists for this frame, use
  // It to get IPP. Otherwise use what is in the SharedFunctionalGroupsSequence/
  if (dataSet.elements.x52009230 && frame) {
    PerFrameFunctionalGroupsSequenceI =
      dataSet.elements.x52009230.items[frame].dataSet;

    const perFramePlanePositionSequence =
      PerFrameFunctionalGroupsSequenceI.elements.x00209113;

    frameImageMetadata.imagePositionPatient = perFramePlanePositionSequence.items[0].dataSet.string(
      'x00200032'
    );
  } else if (SharedFunctionalGroupsSequenceDataSet.elements.x00209113) {
    frameImageMetadata.imagePositionPatient = SharedFunctionalGroupsSequenceDataSet.elements.x00209113.items[0].dataSet.string(
      'x00200032'
    );
  } else {
    frameImageMetadata.imagePositionPatient = 'NaN\\NaN\\NaN';
  }

  const imagePosition = frameImageMetadata.imagePositionPatient.split('\\');

  let planeOrientationSequence = PerFrameFunctionalGroupsSequenceI
    ? PerFrameFunctionalGroupsSequenceI.elements.x00209116
    : null;

  // IF null, use the shared group, if it exists.
  planeOrientationSequence =
    planeOrientationSequence ||
    SharedFunctionalGroupsSequenceDataSet.elements.x00209116;

  if (planeOrientationSequence) {
    frameImageMetadata.imageOrientationPatient = planeOrientationSequence.items[0].dataSet.string(
      'x00200037'
    );
  } else {
    frameImageMetadata.imageOrientationPatient = 'NaN\\NaN\\NaN';
  }

  const imageOrientation = frameImageMetadata.imageOrientationPatient.split(
    '\\'
  );

  let pixelMeasuresSequence = PerFrameFunctionalGroupsSequenceI
    ? PerFrameFunctionalGroupsSequenceI.elements.x00289110
    : null;

  // IF null, use the shared group, if it exists.
  pixelMeasuresSequence =
    pixelMeasuresSequence ||
    SharedFunctionalGroupsSequenceDataSet.elements.x00289110;

  if (pixelMeasuresSequence) {
    frameImageMetadata.pixelSpacing = pixelMeasuresSequence.items[0].dataSet.string(
      'x00280030'
    );
  }

  let columnPixelSpacing = 1.0;

  let rowPixelSpacing = 1.0;

  if (frameImageMetadata.pixelSpacing) {
    const split = frameImageMetadata.pixelSpacing.split('\\');

    rowPixelSpacing = parseFloat(split[0]);
    columnPixelSpacing = parseFloat(split[1]);
  }

  return {
    frameOfReferenceUID: frameImageMetadata.frameOfReferenceUID,
    rows: frameImageMetadata.rows,
    columns: frameImageMetadata.columns,
    rowCosines: [
      parseFloat(imageOrientation[0]),
      parseFloat(imageOrientation[1]),
      parseFloat(imageOrientation[2]),
    ],
    columnCosines: [
      parseFloat(imageOrientation[3]),
      parseFloat(imageOrientation[4]),
      parseFloat(imageOrientation[5]),
    ],
    imagePositionPatient: [
      parseFloat(imagePosition[0]),
      parseFloat(imagePosition[1]),
      parseFloat(imagePosition[2]),
    ],
    pixelSpacing: frameImageMetadata.pixelSpacing,
    rowPixelSpacing,
    columnPixelSpacing,
    imageOrientationPatient: frameImageMetadata.imageOrientationPatient,
  };
}

export default metaDataProvider;
